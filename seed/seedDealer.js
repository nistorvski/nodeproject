//Mongo dev
const mongoose = require('mongoose');
//Importante ponerlo con .DB_URL al requierir el fichero, si no da error de uri.
const DB_URL = require('../db').DB_URL;
//Modelo
const Concessionaire = require('../models/Concessionaire');

const dealer = [
    {
       name:"Tu coche ya",
       locattion:"Calle Miranda de arga,Hortaleza,Madrid"
    },
    {
        name:"Tu otro coche nuevo ya",
       locattion:"Calle Felipe II ,Prosperidad,Madrid"
    },
    {
        name:"coche, tu coche, tu coche, tu coche ",
        locattion:"Calle Plm de fmm,Coslada,Madrid"
    }
];

const dealerDocuments = dealer.map(dealer => new Concessionaire(dealer));
console.log('all carsDocuments', dealerDocuments);


mongoose
  .connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
.then( async ()=> {
    const allConcessionaire = await Concessionaire.find();

   

    if(allConcessionaire.length) {
        await Concessionaire.collection.drop();
    }
})
.catch((err)=> {
    console.log(`Error deleting db data ${err}`);
})
.then( async()=> {
    await Concessionaire.insertMany(dealerDocuments);
    console.log('Seed saved in DB');
})
.catch((err) => {
    console.log(`Error adding data to our db ${err}`);
})

.finally(()=> 
    mongoose.disconnect()
);

