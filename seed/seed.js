//Mongo dev
const mongoose = require('mongoose');
//Importante ponerlo con .DB_URL al requierir el fichero, si no da error de uri.
const DB_URL = require('../db').DB_URL;
//Modelo
const Car = require('../models/Cars');

const cars = [
    {
        brand:"Toyota",
        model:"Yaris",
        color:"Red",
        hp:120,
        km:80,
        weight:"3500",
        gear_box:"h",
        fuel:"Diesel",
        price: 20000,
    },
    {
        brand:"Toyota",
        model:"Land Crusier",
        color:"Blue",
        hp:204,
        km:0,
        weight:"3500",
        gear_box:"h",
        fuel:"Diesel",
        price:46099
    },
    {
        brand:"BMW",
        model:"x5",
        color:"Blue",
        hp:204,
        km:0,
        weight:"3500",
        gear_box:"h",
        fuel:"Diesel",
        price:36099
    }
];

const carsDocuments = cars.map(car => new Car(car));
console.log('all carsDocuments', carsDocuments);


mongoose
  .connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
.then( async ()=> {
    const allCars = await Car.find();

   

    if(allCars.length) {
        await Car.collection.drop();
    }
})
.catch((err)=> {
    console.log(`Error deleting db data ${err}`);
})
.then( async()=> {
    await Car.insertMany(carsDocuments);
    console.log('Seed saved in DB');
})
.catch((err) => {
    console.log(`Error adding data to our db ${err}`);
})

.finally(()=> 
    mongoose.disconnect()
);