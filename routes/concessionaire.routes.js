const express = require('express');
const router = express.Router();

const dealerController = require('../controllers/concessionaire.controller');

router.get('/', dealerController.dealerGet);
router.get('/:id',dealerController.dealerId);


module.exports = router;