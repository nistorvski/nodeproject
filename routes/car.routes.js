const express = require('express');

const carController = require('../controllers/car.controller');
const router = express.Router();
const authMiddleware = require('../middlewares/auth.middleware');
const fileMiddlewares = require('../middlewares/file.middleware');


//List Car
router.get('/', carController.carGet);
//Add Car Form
router.get('/create',[authMiddleware.isAuthenticated], carController.carCreateGet);
router.post('/create',[authMiddleware.isAuthenticated],[fileMiddlewares.upload.single('image'), fileMiddlewares.uploadToCloudinary], carController.carPost );
//Edit Car Form
router.get('/edit/:id',[authMiddleware.isAuthenticated], carController.carEditGet);
router.post('/edit/:id',[authMiddleware.isAuthenticated],[fileMiddlewares.upload.single('image'), fileMiddlewares.uploadToCloudinary], carController.carEdit);
//Car Detail
router.get('/:id',[authMiddleware.isAuthenticated], carController.carDetail);


module.exports = router;

