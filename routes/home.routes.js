const express = require('express');

const router = express.Router();


router.get('/', (req, res, next)=> {
    return res.render('home');
});

router.get('/register', (req, res, next)=> {
    return res.render('register', {title: 'Register User'});
}
);

router.get('/login', (req, res, next) => {
    res.render('login');
  });

module.exports = router;