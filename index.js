require('dotenv').config();
const express = require('express');
const passport = require('passport');
const path = require('path');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);


const PORT = process.env.PORT || 5555;
const app = express();
const db = require('./db');
db.connect();
require('./passport');

const homePage=require('./routes/home.routes');
const userRoutes=require('./routes/user.routes');
const carRoutes= require('./routes/car.routes');
const dealerRoutes=require('./routes/concessionaire.routes');



//Passport

app.use(
    session({
      secret: process.env.SESSION_SECRET || 'upgradehub_node', // ¡Este secreto tendremos que cambiarlo en producción!
      resave: false, // Solo guardará la sesión si hay cambios en ella.
      saveUninitialized: false, // Lo usaremos como false debido a que gestionamos nuestra sesión con Passport
      cookie: {
        maxAge: 3600000 // Milisegundos de duración de nuestra cookie, en este caso será una hora.
      },
      store: new MongoStore({ mongooseConnection: mongoose.connection }),
    })
  );

app.use(passport.initialize());
app.use(passport.session());



//hbs-Vistas html
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');


app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.use(express.static(path.join(__dirname, 'public')));


//Rutas
app.use('/', homePage);
app.use('/user', userRoutes);
app.use('/car', carRoutes);
app.use('/concessionaire', dealerRoutes);


app.use('*', (req, res, next)=> {
    const error = new Error('Route not found');
    error.status=404;
    next(error);
});


app.use((err, req, res, next)=> {
    return res.status(err.status || 500).render('error', {
        message: err.message || 'Unexpected error',
        status: err.status || 500,
    });
});


app.listen(PORT, () => {
    console.log(`The server is listening in localhost://${PORT}`);
});