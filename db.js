require('dotenv').config();
const mongoose = require('mongoose');

const DB_URL = `mongodb+srv://Lucian:udJP20wPFfOIKoPO@cluster0.tgeor.mongodb.net/Cluster0?retryWrites=true&w=majority`;

//const DB_URL = 'mongodb://localhost:27017/node_project';



// Función que conecta a nuestra base de datos
const connect = () => {
    // método que nos da mongoose para conectar a nuestra DB.
    mongoose.connect(DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(() => {
        // Si todo va bien conectando con la DB, se ejecuta
        console.log('Success connecteded to DB');
    })
    .catch(error => {
        // Si hay algún error, se ejecuta esta línea
        console.log('Error connecting to DB: ', error);
    })
}

// Exportamos este archivo como un objeto con dos propiedades, la función para conectar y la URL de nuestra DB
module.exports = { DB_URL, connect };
