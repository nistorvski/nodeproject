const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const concessionaireSchema = new Schema ({
    name:{
        type:String,
        required:true,
    },
    locattion:{
        type:String,
        required:true,
    },
    cars:[
        {
            type:mongoose.Types.ObjectId,
            ref:'Car'
        }
    ]
});

const Concessionaire = mongoose.model('Concessionaire', concessionaireSchema);
module.exports = Concessionaire ;