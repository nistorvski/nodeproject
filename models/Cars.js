const mongoose=require('mongoose');
const Schema = mongoose.Schema;



const carSchema = new Schema({
    
    brand:{
        type:String,
        required:true,
    },
    model:{
        type:String,
        required:true,
    },
    color:{
        type:String
    },
    hp:{
        type:Number
    },
    km:{
        type:Number
    },
    weight:{
        type:String,
    },
    gear_box:{
        enum:['handbook','automatic'],
        type:String,
        required:true,
    },
    fuel:{
        type:String
    },
    price:{
        type:Number,
        required:true,
    },
    image: {
        type:String,
    }
});

const Car = mongoose.model('Car',carSchema);
module.exports = Car;