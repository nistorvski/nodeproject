const Concessionaire = require ('../models/Concessionaire');

const controller = {};


controller.dealerGet = async (req, res, next) =>{
    try{

        const dealer = await Concessionaire.find().populate({path:'cars'});
        console.log('Dealerr get', dealer.cars);
        
        res.status(200).render('concessionaire', {title:'Concessionaire page', dealer});

    }catch(error){
        return next(error);
    }

};

controller.dealerId = async(req, res, next) => {
    try{
        const id = req.params.id;

        const findDealer = await Concessionaire.findById(id).populate('cars');

        console.log('findeDealerpopulate', findDealer.cars);

        return res.status(200).render('dealerDetail', {title:'Dealer Detail', dealer: findDealer});

    }catch(error){
        return next(error)
    }
}

module.exports = controller;