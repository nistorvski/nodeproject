
const passport = require('passport');


const controller = {};


controller.userRegister = (req, res, next)=>{
    passport.authenticate('register', (error, user)=> {
        if(error) {
            return res.render('register', { error: error.message});
        }

        req.logIn(user, (error) => {
            if(error){
                return res.render('register', { error: error.message });
            }
            
                 return res.redirect('/')
        });


    })(req, res, next);
};

controller.userLogin = (req, res, next)=> {
    passport.authenticate('login', (error, user) => {
        if(error){
            return res.render('login', { error: error.message });
        }

        req.logIn(user, (error) => {
            if(error){
                return res.render('login', { error: error.message });
            }
            return res.redirect('/');
        });

        
    })(req, res, next);
};

controller.userLogout =  (req, res, next)=> {
    if(req.user) {
        req.logout();

        req.session.destroy(() => {
            res.clearCookie('connect.sid');
            res.redirect('/');

        });
    }else{
        return res.sendStatus(304);
    }
};

module.exports = controller;