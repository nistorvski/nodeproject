const Car = require('../models/Cars');
const uploadToCloudinary = require('../middlewares/file.middleware');

const controller =  {};


controller.carGet = async (req, res, next)=> {
    try{
        const cars = await Car.find();

        return res.status(200).render('cars', {title:'Cars List Page', cars});

    }catch(error){
        return next(error);
    }
};

controller.carCreateGet = (req, res, next)=> {
    
    try{
        res.status(200).render('carForm', {title: 'Car Form'});
    
    }catch(error){
        return next(error);
    }
  
};

controller.carPost =async (req, res, next)=> {

    const carImage = req.file_url || null;

    try{    
        const {
            brand,
            model,
            color,
            hp,
            gear_box,
            km,
            weight,
            fuel,
            price,
            image
        } = req.body;

        const newCar = new Car({
            brand,
            model,
            color,
            hp:Number(hp),
            gear_box,
            km:Number(km),
            weight,
            fuel,
            price:Number(price),
            image: carImage
        });
        
        await newCar.save();

        return res.status(201).redirect('/car');

    }catch(error){
        return next(error);
    }
    
};


controller.carEditGet = async (req, res, next) => {
    const id = req.params.id;
    try{       

        let catchCar = await Car.findById(id);

        if(!catchCar){
            catchCar = false;
        }

       // console.log('catch Caaar', catchCar.id);

        res.status(200).render('carEdit', { title:'Page Car Edit', car: catchCar, id });

    }catch(error){
        return next(error);
    }
}

controller.carEdit = async (req, res, next) => {
    try{

        const carImage = req.file ? req.file.filename : null ;
        const id = req.params.id;

        const {
            brand,
            model,
            color,
            hp,
            gear_box,
            km,
            weight,
            fuel,
            price,
            image,
        } = req.body;

       const carEdited = await Car.findByIdAndUpdate(id,
            {
                brand,
                model,
                color,
                hp:Number(hp),
                gear_box,
                km:Number(km),
                weight,
                fuel,
                price:Number(price),
                image: carImage
            },
            {new:true}
            );

            console.log('CAr editeed', carEdited)
            
            //return res.redirect(`/car/${carEdited.id}`);
            
            return res.status(200).json(carEdited);

    }catch(error){
        return next(error);
    }
};

controller.carDetail = async (req, res, next)=> {
    try{
        const id = req.params.id;

        const car = await Car.findById(id);

        return res.status(200).render('carDetail', { title:'Car Detail', car });

    }catch(error){
        return next(error);
    }
};



module.exports = controller;